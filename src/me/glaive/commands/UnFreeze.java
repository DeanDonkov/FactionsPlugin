package me.glaive.commands;

import me.glaive.Main;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class UnFreeze implements CommandExecutor {
    @Override
    public boolean onCommand (CommandSender cs, Command cmd, String label, String[] args) {
        if(!(cs instanceof Player)){
            System.out.println(Main.getInstance().color("&cConsole can't execute commands!"));
        }
        Player p = (Player) cs;
        if(cmd.getName().equalsIgnoreCase("unfreeze")){
            if(cs.hasPermission("Server.unfreeze")){
                if(args.length == 0) {
                    p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("UNFREEZE_NO_ARGS")));
                    return true;
                }
                if(args.length == 1) {
                    Player target = Bukkit.getPlayer(args[0]);
                    if(target == null) {
                        p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("UNFREEZE_PLAYER_OFFLINE").replace("%target%", target.getName())));
                        return true;
                    }
                    if(!(Main.getInstance().FrozenPlayers.contains(target.getUniqueId()))){
                        p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("UNFREEZE_PLAYER_NOT_FROZEN")).replace("%target%", target.getName()));
                        return true;
                    }
                    Main.getInstance().FrozenPlayers.remove(target.getUniqueId());
                    p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("UNFREEZE_SUCCESSFULLY_UNFROZEN")).replace("%target%", target.getName()));
                    return true;
                }
                if(args.length >= 2){
                    p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("UNFREEZE_NO_ARGS")));
                    return true;
                }
            } else {
                p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("NO-PERM")));
                return true;
            }
        }
        return true;
    }
}
