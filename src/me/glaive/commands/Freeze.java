package me.glaive.commands;

import me.glaive.Main;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Freeze implements CommandExecutor {
    @Override
    public boolean onCommand (CommandSender cs, Command cmd, String label, String[] args) {
        if(!(cs instanceof Player)){
            System.out.println(Main.getInstance().color("&cConsole can't write commands!"));
        }
        Player p = (Player) cs;
        if(cmd.getName().equalsIgnoreCase("freeze")){
            if(p.hasPermission("Server.freeze")){
                if(args.length == 0) {
                    p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("FREEZE_NO_ARGS")));
                    return true;
                }
                if(args.length == 1) {
                    Player target = Bukkit.getPlayer(args[0]);
                    if(target == null) {
                        p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("FREEZE_PLAYER_OFFLINE")).replace("%target%", args[0]));
                        return true;
                    }
                    if(Main.getInstance().FrozenPlayers.contains(target.getUniqueId())){
                        p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("FREEZE_ALREADY_FROZEN")).replace("%target%", target.getName()));
                        return true;
                    }
                    Main.getInstance().FrozenPlayers.add(target.getUniqueId());
                    p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("SUCCESSFULLY_FROZEN_PLAYER").replace("%target%", target.getName())));
                }
                if(args.length >= 2) {
                 p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("FREEZE_NO_ARGS")));
                 return true;
                }
            } else {
                p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("NO-PERM")));
                return true;
            }
        }
        return true;
    }
}
