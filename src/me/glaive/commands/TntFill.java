package me.glaive.commands;

import me.glaive.Main;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Dispenser;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class TntFill implements CommandExecutor {

    public boolean onCommand(final CommandSender cs, final Command cmd, final String cl, final String[] args) {
        if (cl.equalsIgnoreCase("tntfill")) {
            if (!(cs instanceof Player)) {
                System.out.println(Main.getInstance().color("&cConsole can't write commands!"));
            }
                final Player p = (Player)cs;
                if (p.hasPermission("Server.tntfill")) {
                    if (p.getInventory().contains(Material.TNT)) {
                        final Block b = p.getWorld().getBlockAt(p.getLocation());
                        final List<Dispenser> dispensers = new ArrayList<Dispenser>();
                        for (int x = -Main.getInstance().getConfig().getInt("x-int"); x <= Main.getInstance().getConfig().getInt("TNTFILL-X"); ++x) {
                            for (int y = -Main.getInstance().getConfig().getInt("y-int"); y <= Main.getInstance().getConfig().getInt("TNTFILL-Y"); ++y) {
                                for (int z = -Main.getInstance().getConfig().getInt("z-int"); z <= Main.getInstance().getConfig().getInt("TNTFILL-Z"); ++z) {
                                    final Block br = b.getRelative(x, y, z);
                                    if (br.getType().equals((Object) Material.DISPENSER)) {
                                        dispensers.add((Dispenser)br.getState());
                                    }
                                }
                            }
                        }
                        if (dispensers.isEmpty()) {
                            p.sendMessage(Main.getInstance().color( Main.getInstance().getConfig().getString("TNTFILL_NO_DISPENSERS")));
                            return true;
                        }
                        int tntCount = 0;
                        ItemStack[] contents;
                        for (int length = (contents = p.getInventory().getContents()).length, k = 0; k < length; ++k) {
                            final ItemStack i = contents[k];
                            if (i != null) {
                                if (i.getType().equals((Object)Material.TNT)) {
                                    tntCount += i.getAmount();
                                }
                            }
                        }
                        p.getInventory().remove(Material.TNT);
                        int j = tntCount;
                        while (j > 0) {
                            for (final Dispenser d : dispensers) {
                                if (j <= 0) {
                                    break;
                                }
                                d.getInventory().addItem(new ItemStack[] { new ItemStack(Material.TNT) });
                                --j;
                            }
                        }
                        p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("TNTFILL_DISPENSERS_FILLED")));
                    }
                    else {
                        p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("TNTFILL_NO_TNT")));
                    }
                }
                else {
                    p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("NO-PERM")));
                    return true;
                }
            }
        return true;
            }
        }
