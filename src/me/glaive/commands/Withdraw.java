package me.glaive.commands;

import me.glaive.Main;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;

public class Withdraw implements CommandExecutor
{

    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("§cYou must be a player to use this command!");
        }
        if (sender instanceof Player) {
            final Player p = (Player)sender;
            if (args.length == 0) {
                p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("WITHDRAW_NO_ARGS")));
            }
            else if (args.length == 1) {
                if (this.is0(args[0])) {
                    p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("ATTEMPT_TO_WITHDRAW_0")));
                }
                if (!this.isAlpha(args[0])) {
                    p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("WITHDRAW_NO_ARGS")));
                }
                else if (this.isAlpha(args[0]) && !this.is0(args[0])) {
                    final String title = Main.getInstance().getConfig().getString("WITHDRAW_TITLE");
                    final String translate = ChatColor.translateAlternateColorCodes('&', title);
                    final String lore = Main.getInstance().getConfig().getString("WITHDRAW_LORE_1");
                    final String translateL = ChatColor.translateAlternateColorCodes('&', lore);
                    final String wm = Main.getInstance().getConfig().getString("WITHDRAW_WITHDREW").replace("%MONEY", args[0]);
                    final String wmL = ChatColor.translateAlternateColorCodes('&', wm);
                    final double args2 = new Double(args[0]);
                    final ItemStack note = new ItemStack(Material.PAPER);
                    final ItemMeta noteM = note.getItemMeta();
                    noteM.setDisplayName(translate);
                    noteM.setLore((List)Arrays.asList(String.valueOf(translateL) + args[0]));
                    note.setItemMeta(noteM);
                    final double money1 = Main.getInstance().economy.getBalance((OfflinePlayer)p);
                    if (money1 >= args2) {
                        p.getInventory().addItem(new ItemStack[] { note });
                        Main.getInstance().economy.withdrawPlayer((OfflinePlayer)p, args2);
                        p.sendMessage(String.valueOf(wmL));
                    }
                    else if (money1 < args2) {
                        p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("WITHDRAW_NO_MONEY")));
                    }
                }
            }
        }
        return false;
    }

    public boolean isAlpha(final String name) {
        return name.matches("[0-9.]+");
    }

    public boolean is0(final String name) {
        return name.matches("[0]+");
    }
}

