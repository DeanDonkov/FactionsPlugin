package me.glaive.commands;

import me.glaive.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Cobble implements CommandExecutor {
    @Override
    public boolean onCommand (CommandSender cs, Command cmd, String label, String[] args) {
        if(!(cs instanceof Player)){
            System.out.println(Main.getInstance().color("&cConsole can't write commands!"));
        }
        Player p = (Player) cs;
        if(cmd.getName().equalsIgnoreCase("cobble")){
          if(args.length == 0) {
              if(Main.getInstance().CobblePlayers.contains(p.getUniqueId())){
                  Main.getInstance().CobblePlayers.remove(p.getUniqueId());
                  p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("COBBLE_EXITED")));
                  return true;
              }
              if(!(Main.getInstance().CobblePlayers.contains(p.getUniqueId()))){
                  Main.getInstance().CobblePlayers.add(p.getUniqueId());
                  p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("COBBLE_ENTERED")));
                  return true;
              }
          }
          if(args.length >= 2) {
              p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("COBBLE_TOO_MANY_ARGS")));
          }
        }


        return true;
    }
}
