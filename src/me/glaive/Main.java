package me.glaive;

import me.glaive.commands.*;
import me.glaive.listeners.*;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class Main extends JavaPlugin{

    public static Economy economy = null;

    public ArrayList<UUID> FrozenPlayers = new ArrayList<UUID>();
    public ArrayList<UUID> CobblePlayers = new ArrayList<UUID>();

    public HashMap<UUID, Long> GappleCoolDown = new HashMap<UUID, Long>();
    public HashMap<UUID, Long> EnderPearlCoolDown = new HashMap<UUID, Long>();

    private static Main instance;

    public Main() {
        instance = this;
    }

    public static Main getInstance() {
        return instance;
    }


    public void onEnable(){
        this.getConfig().options().copyDefaults(true);
        if (!setupEconomy()) {
            System.out.println("Vault wasn't found! Please install Vault and then run the plugin!");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
        this.saveDefaultConfig();
        AntiLootSteal.usesDamage = this.getConfig().getBoolean("ANTILOOTSTEAL_DAMAGECHECK");
        this.getServer().getPluginManager().registerEvents(new AntiLootSteal(), this);
        if (AntiLootSteal.usesDamage) {
            new BukkitRunnable() {
                public void run() {
                    for (final UUID player : AntiLootSteal.damagers.keySet()) {
                        if (AntiLootSteal.damagers.get(player).elapsed()) {
                            AntiLootSteal.damagers.remove(player);
                        }
                    }
                }
            }.runTaskTimer(this, 20L, 20L);
        }
        this.getCommand("cobble").setExecutor(new Cobble());
        this.getCommand("withdraw").setExecutor(new Withdraw());
        this.getCommand("freeze").setExecutor(new Freeze());
        this.getCommand("unfreeze").setExecutor(new UnFreeze());
        this.getCommand("tntfill").setExecutor(new TntFill());
        Bukkit.getPluginManager().registerEvents(new FreezeListener(),this);
        Bukkit.getPluginManager().registerEvents(new LavaSponge(), this);
        Bukkit.getPluginManager().registerEvents(new CobbleEvent(), this);
        Bukkit.getPluginManager().registerEvents(new EnderPearlCD(), this);
        Bukkit.getPluginManager().registerEvents(new GappleCD(), this);
        Bukkit.getPluginManager().registerEvents(new WithdrawListener(), this);
    }
    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        economy = rsp.getProvider();
        return economy != null;
    }
    public String color(String a){
        return ChatColor.translateAlternateColorCodes('&', a);
    }


}
