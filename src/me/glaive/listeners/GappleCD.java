package me.glaive.listeners;

import me.glaive.Main;
import org.apache.commons.lang.time.DurationFormatUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.text.DecimalFormat;

public class GappleCD implements Listener {
    ItemStack enchantedApple;

    public GappleCD () {
        this.enchantedApple = new ItemStack(Material.GOLDEN_APPLE, 1, (short) 1);
    }

    int GappleCD = Main.getInstance().getConfig().getInt("GappleCD");

    @EventHandler
    public void ItemConsume (PlayerItemConsumeEvent e) {
        Player p = (Player) e.getPlayer();
        if ((e.getItem().getType() == enchantedApple.getType()) && e.getItem().getDurability() == enchantedApple.getDurability()){
            if (!Main.getInstance().GappleCoolDown.containsKey(p.getUniqueId())) {
                Main.getInstance().GappleCoolDown.put(p.getUniqueId(), System.currentTimeMillis());
                final long timeleft = Main.getInstance().GappleCoolDown.get(p.getUniqueId()) + GappleCD * 1000 - System.currentTimeMillis();
                if (timeleft > 0L) {
                    p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("GAPPLE_CD_MESSAGE").replace("%cooldown%", DurationFormatUtils.formatDuration(timeleft, "s.S"))));
                    return;
                }
            } if(Main.getInstance().GappleCoolDown.containsKey(p.getUniqueId())){
                long timeleft = Main.getInstance().GappleCoolDown.get(p.getUniqueId()) + GappleCD * 1000 - System.currentTimeMillis();
                e.setCancelled(true);
                if(timeleft > 0L){
                    p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("GAPPLE_CD_MESSAGE").replace("%cooldown%", DurationFormatUtils.formatDuration(timeleft, "s.S"))));
                    return;
                }
                Main.getInstance().GappleCoolDown.remove(p.getUniqueId());
            }
        }
    }

    }