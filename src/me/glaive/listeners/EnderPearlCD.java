package me.glaive.listeners;

import me.glaive.Main;
import org.apache.commons.lang.time.DurationFormatUtils;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.text.DecimalFormat;

public class EnderPearlCD implements Listener {



    @EventHandler
    public void onPearlThrow(final PlayerInteractEvent e) {
        final Player p = e.getPlayer();
        if ((e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) && p.getItemInHand() != null && p.getItemInHand().getType() == Material.ENDER_PEARL && (p.getGameMode() == GameMode.SURVIVAL || p.getGameMode() == GameMode.ADVENTURE)) {
            if (Main.getInstance().EnderPearlCoolDown.containsKey(p.getUniqueId())) {
                final int seconds = Main.getInstance().getConfig().getInt("EnderPearlCD");
                final long timeleft = Main.getInstance().EnderPearlCoolDown.get(p.getUniqueId()) + seconds * 1000 - System.currentTimeMillis();
                if (timeleft > 0L) {
                    p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("ENDERPEARL_CD_MESSAGE").replace("%cooldown%", DurationFormatUtils.formatDuration(timeleft, "s.S"))));
                    e.setCancelled(true);
                    return;
                }
            }
            Main.getInstance().EnderPearlCoolDown.put(p.getUniqueId(), System.currentTimeMillis());
        }
    }
}
