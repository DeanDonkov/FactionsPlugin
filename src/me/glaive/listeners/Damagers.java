package me.glaive.listeners;

import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class Damagers {

    protected Long lasthit;
    private HashMap<UUID, Integer> damagers;

    public Damagers() {
        this.damagers = new HashMap<UUID, Integer>();
    }

    public HashMap<UUID, Integer> getDamagers() {
        return this.damagers;
    }

    public void addDamage(final Player player, final int damage) {
        if (this.damagers.containsKey(player.getUniqueId())) {
            this.damagers.put(player.getUniqueId(), this.damagers.get(player.getUniqueId()) + damage);
        }
        else {
            this.damagers.put(player.getUniqueId(), damage);
        }
        this.lasthit = System.currentTimeMillis();
    }

    public UUID getTopDamager() {
        UUID player = (UUID)this.damagers.keySet().toArray()[0];
        for (final UUID damager : this.damagers.keySet()) {
            if (this.damagers.get(damager) > this.damagers.get(player)) {
                player = damager;
            }
        }
        return player;
    }

    public boolean elapsed() {
        return System.currentTimeMillis() - this.lasthit >= 30000L;
    }
}
