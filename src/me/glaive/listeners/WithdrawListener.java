package me.glaive.listeners;

import me.glaive.Main;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.ChatColor;
import org.bukkit.event.player.*;
import org.bukkit.entity.*;
import org.bukkit.event.block.*;
import net.md_5.bungee.api.*;
import org.bukkit.inventory.*;
import org.bukkit.*;
import org.bukkit.event.*;

public class WithdrawListener implements Listener
{


    @EventHandler
    public void onNoteClick(final PlayerInteractEvent e) {
        final Player p = e.getPlayer();
        final Action action = e.getAction();
        if (e.getItem() == null) {
            return;
        }
        if (e.getItem().getItemMeta().getDisplayName() == null) {
            return;
        }
        if ((action == Action.RIGHT_CLICK_BLOCK || action == Action.RIGHT_CLICK_AIR) && e.getItem().getItemMeta().getDisplayName() != null) {
            final ItemStack hand = p.getItemInHand();
            final String title = Main.getInstance().getConfig().getString("WITHDRAW_TITLE");
            final String translate = ChatColor.translateAlternateColorCodes('&', title);
            final String rm = Main.getInstance().getConfig().getString("WITHDRAW_WITHDREW");
            final String rmL = ChatColor.translateAlternateColorCodes('&', rm);
            final String lo = Main.getInstance().getConfig().getString("WITHDRAW_LORE_1");
            final String lol = ChatColor.translateAlternateColorCodes('&', lo);
            if (!hand.hasItemMeta()) {
                return;
            }
            if (hand.getType() == Material.AIR) {
                return;
            }
            if (!hand.getItemMeta().hasLore()) {
                return;
            }
            if (hand.getItemMeta().getDisplayName() == null) {
                return;
            }
            final double moneyd = new Double(hand.getItemMeta().getLore().get(0).replace(lol, ""));
            final String name = e.getItem().getItemMeta().getDisplayName();
            if (name.equalsIgnoreCase(translate) && hand.getType().equals((Object)Material.PAPER)) {
                e.setCancelled(true);
                if (hand.getAmount() != 1) {
                    p.getItemInHand().setAmount(p.getItemInHand().getAmount() - 1);
                }
                else {
                    p.getInventory().remove(hand);
                }
                Main.getInstance().economy.depositPlayer((OfflinePlayer)p, moneyd);
                p.sendMessage(Main.getInstance().color(Main.getInstance().getConfig().getString("WITHDRAW_WITHDREW")).replace("%MONEY%", String.valueOf(moneyd)));
            }
        }
    }
}
