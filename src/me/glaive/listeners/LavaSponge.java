package me.glaive.listeners;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.ArrayList;
import java.util.List;

public class LavaSponge implements Listener {

    private void clearSurroundingLava(final Block sponge) {
        List<Block> lavas = this.getNearest(sponge, Material.LAVA);
        if (!lavas.isEmpty()) {
            for (final Block lava : lavas) {
                lava.setType(Material.AIR);
            }
        }
        lavas = this.getNearest(sponge, Material.STATIONARY_LAVA);
        if (!lavas.isEmpty()) {
            for (final Block lava : lavas) {
                lava.setType(Material.AIR);
            }
        }
    }

    private List<Block> getNearest(final Block block, final Material material) {
        final List<Block> blocks = new ArrayList<Block>();
        for (int x = block.getX() - 3; x < block.getX() + 3; ++x) {
            for (int y = block.getY() - 3; y < block.getY() + 3; ++y) {
                for (int z = block.getZ() - 3; z < block.getZ() + 3; ++z) {
                    final Block near = block.getWorld().getBlockAt(x, y, z);
                    if (near.getType() == material) {
                        blocks.add(near);
                    }
                }
            }
        }
        return blocks;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onLavaFlow(final BlockFromToEvent event) {
        final Block lava = event.getBlock();
        if (lava.getType().equals((Object)Material.LAVA) || lava.getType().equals((Object)Material.STATIONARY_LAVA)) {
            final List<Block> sponges = this.getNearest(lava, Material.SPONGE);
            if (!sponges.isEmpty()) {
                event.setCancelled(true);
                for (final Block sponge : sponges) {
                    this.clearSurroundingLava(sponge);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockPlace(final BlockPlaceEvent event) {
        if (event.isCancelled()) {
            return;
        }
        final Block blockPlaced = event.getBlockPlaced();
        if (blockPlaced.getType().equals((Object)Material.SPONGE)) {
            final Block sponge = event.getBlockPlaced();
            this.clearSurroundingLava(sponge);
        }
        else if (blockPlaced.getType() == Material.LAVA || blockPlaced.getType() == Material.STATIONARY_LAVA) {
            final List<Block> sponges = this.getNearest(blockPlaced, Material.SPONGE);
            if (!sponges.isEmpty()) {
                event.setCancelled(true);
                for (final Block sponge2 : sponges) {
                    this.clearSurroundingLava(sponge2);
                }
            }
        }
    }
}
