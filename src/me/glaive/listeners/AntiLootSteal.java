package me.glaive.listeners;

import me.glaive.Main;
import me.glaive.listeners.Damagers;
import org.bukkit.plugin.java.*;
import org.bukkit.plugin.*;
import org.bukkit.scheduler.*;
import java.util.*;
import org.bukkit.inventory.*;
import org.bukkit.metadata.*;
import org.bukkit.*;
import org.bukkit.event.player.*;
import org.bukkit.event.entity.*;
import org.bukkit.entity.*;
import org.bukkit.event.*;

public class AntiLootSteal implements Listener {

    public static boolean usesDamage;
    public static HashMap<UUID, Damagers> damagers;

    public AntiLootSteal() {
        this.usesDamage = false;
        this.damagers = new HashMap<UUID, Damagers>();
    }

    @EventHandler
    public void onPlayerDeath(final PlayerDeathEvent e) {
        if (!(e.getEntity() instanceof Player) && !(e.getEntity().getKiller() instanceof Player)) {
            return;
        }
        final Player player = e.getEntity();
        if (e.getDrops().isEmpty()) {
            return;
        }
        if (this.damagers.containsKey(player.getUniqueId())) {
            final Player killer = Bukkit.getPlayer(this.damagers.get(player.getUniqueId()).getTopDamager());
            for (final ItemStack is : e.getDrops()) {
                final Entity entity = (Entity)e.getEntity().getWorld().dropItem(e.getEntity().getLocation(), is);
                if (entity.hasMetadata("LootSteal")) {
                    entity.removeMetadata("LootSteal", Main.getInstance());
                }
                entity.setMetadata("LootSteal", (MetadataValue)new FixedMetadataValue(Main.getInstance(), (Object)(killer.getUniqueId().toString() + " " + System.currentTimeMillis())));
            }
            new BukkitRunnable() {
                public void run() {
                    killer.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7Your Loot Is No Longer Protected."));
                }
            }.runTaskLater(Main.getInstance(), Main.getInstance().getConfig().getInt("ANTILOOTSTEAL_TICKS"));
        }
        else {
            final Player killer = e.getEntity().getKiller();
            for (final ItemStack is : e.getDrops()) {
                final Entity entity = (Entity)e.getEntity().getWorld().dropItem(e.getEntity().getLocation(), is);
                if (entity.hasMetadata("LootSteal")) {
                    entity.removeMetadata("LootSteal", Main.getInstance());
                }
                entity.setMetadata("LootSteal", (MetadataValue)new FixedMetadataValue(Main.getInstance(), (Object)(killer.getUniqueId().toString() + " " + System.currentTimeMillis())));
            }
            new BukkitRunnable() {
                public void run() {
                    killer.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7Your Loot Is No Longer Protected."));
                }
            }.runTaskLater(Main.getInstance(), Main.getInstance().getConfig().getInt("ANTILOOTSTEAL_TICKS"));
        }
        e.getDrops().clear();
    }

    @EventHandler
    public void onItemPickup(final PlayerPickupItemEvent e) {
        if (!e.getItem().hasMetadata("LootSteal")) {
            return;
        }
        final String value = e.getItem().getMetadata("LootSteal").get(0).asString();
        final String[] values = value.split(" ");
        if (e.getPlayer().getUniqueId().toString().equals(values[0])) {
            return;
        }
        if (System.currentTimeMillis() - Long.valueOf(values[1]) >= 10000L) {
            return;
        }
        e.setCancelled(true);
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
    public void onEntityDamage(final EntityDamageByEntityEvent event) {
        if (!this.usesDamage) {
            return;
        }
        if (!(event.getEntity() instanceof Player)) {
            return;
        }
        if (event.getDamage() == 0.0) {
            return;
        }
        final Player player = (Player)event.getEntity();
        Player damager = null;
        if (event.getDamager() instanceof Arrow) {
            final Arrow arrow = (Arrow)event.getDamager();
            if (!(arrow.getShooter() instanceof Player)) {
                return;
            }
            damager = (Player)arrow.getShooter();
        }
        else if (event.getDamager() instanceof Snowball) {
            final Snowball snowball = (Snowball)event.getDamager();
            if (!(snowball.getShooter() instanceof Player)) {
                return;
            }
            damager = (Player)snowball.getShooter();
        }
        else if (event.getDamager() instanceof Egg) {
            final Egg egg = (Egg)event.getDamager();
            if (!(egg.getShooter() instanceof Player)) {
                return;
            }
            damager = (Player)egg.getShooter();
        }
        else {
            if (!(event.getDamager() instanceof Player)) {
                return;
            }
            damager = (Player)event.getDamager();
        }
        if (!this.damagers.containsKey(player.getUniqueId())) {
            this.damagers.put(player.getUniqueId(), new Damagers());
        }
        this.damagers.get(player.getUniqueId()).addDamage(damager, (int)event.getDamage());
    }
}
