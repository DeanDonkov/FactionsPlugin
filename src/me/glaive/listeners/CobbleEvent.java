package me.glaive.listeners;

import me.glaive.Main;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class CobbleEvent implements Listener {

    @EventHandler
    public void Break(PlayerPickupItemEvent e){
        Player p = (Player) e.getPlayer();
        if(e.getItem().getItemStack().getType().equals(Material.COBBLESTONE) || e.getItem().getItemStack().getType().equals(Material.STONE)){
            if(Main.getInstance().CobblePlayers.contains(p.getUniqueId())){
               e.setCancelled(true);
            }
        }
    }
}
