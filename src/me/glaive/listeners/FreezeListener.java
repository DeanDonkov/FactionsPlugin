package me.glaive.listeners;

import me.glaive.Main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.Arrays;
import java.util.List;

public class FreezeListener implements Listener {

    @EventHandler
    public void Freeze(PlayerMoveEvent e) {
        Player p = (Player) e.getPlayer();
        if(Main.getInstance().FrozenPlayers.contains(p.getUniqueId())){
            e.setCancelled(true);
            List<String> messages = Main.getInstance().getConfig().getStringList("FROZEN_MESSAGES");
            for ( String s : messages) {
                p.sendMessage(Main.getInstance().color(s.toString()));
            }
        }
    }
}
